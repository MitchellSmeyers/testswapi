import './App.css';

function PlanetDetailOverlay(planet) {
    console.log('LOGGING PLANET ', planet);


    //todo make this an actually popup window/overlay with css
    //did not have time for notable people
        return (
            <div>
                <h1>{planet.name}</h1>
                <h2>Terrain: {planet.terrain}</h2>
                <h2>Climate: {planet.climate}</h2>
                <h2>Gravity: {planet.gravity}</h2>
            </div>
        )
}

export default PlanetDetailOverlay;
