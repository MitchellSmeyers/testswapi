import './App.css';
import {useState, useEffect} from "react";
import PlanetDetailOverlay from "./PlanetDetailOverlay";

function App() {
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [planets, setPlanets] = useState([]);
    const [showPlanetDetail, setShowPlanetDetail] = useState(false);
    const [loadPages, setLoadPages] = useState([1]);

    useEffect(() => {
        //todo, when loadpages changes, load new page and add to planets
        fetch("https://swapi.dev/api/planets/")
            .then(res => res.json())
            .then(
                (apiCall) => {
                    setIsLoaded(true);
                    setPlanets(apiCall.results);
                },
                (error) => {
                    setIsLoaded(true);
                    setError(error);
                }
            )
    }, [])

    const PlanetCard = (planet) => {
        return (
        <div className="Planet-card" onClick={() => setShowPlanetDetail( true)} key={planet.url}>
            <div>
                <h1>{planet.name}</h1>
                <h2>{planet.terrain}</h2>
                <p>Population: {planet.population}</p>
            </div>
        </div>
        )
    }

    console.log(planets);

    if (error) {
        return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
        return <div>Loading...</div>;
    } else {
        return (
            <div className="App">
                <header className="App-header">
                    {showPlanetDetail ? <PlanetDetailOverlay/> : null}
                    <div className="Planet-container">
                    {planets.map(planet => {
                        return PlanetCard(planet)
                    })}
                    </div>
                    {/* todo make button load more planets */}

                    <button onClick={() => console.log('setloadpages +1, fetch extra page from api, add new page to planets')}>Load more</button>
                </header>
            </div>
        );
    }
}

export default App;
